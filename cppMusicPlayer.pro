#-------------------------------------------------
#
# Project created by QtCreator 2014-07-21T13:02:41
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cppMusicPlayer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    playlist.cpp \
    track.cpp

HEADERS  += mainwindow.h \
    playlist.h \
    track.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc

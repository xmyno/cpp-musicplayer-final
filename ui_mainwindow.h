/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *musicPlayer;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QPushButton *upBtn;
    QPushButton *downBtn;
    QSpacerItem *verticalSpacer_2;
    QPushButton *addBtn;
    QPushButton *removeBtn;
    QPushButton *saveBtn;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout_3;
    QTableWidget *trackList;
    QHBoxLayout *playerControls;
    QPushButton *playBtn;
    QSpacerItem *horizontalSpacerL;
    QPushButton *prevBtn;
    QPushButton *nextBtn;
    QSpacerItem *horizontalSpacerR;
    QPushButton *muteBtn;
    QSlider *volumeSlider;
    QLabel *volumeLbl;
    QHBoxLayout *durationBox;
    QLabel *progressLbl;
    QSlider *progressSlider;
    QLabel *durationLbl;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(490, 479);
        musicPlayer = new QWidget(MainWindow);
        musicPlayer->setObjectName(QStringLiteral("musicPlayer"));
        verticalLayout = new QVBoxLayout(musicPlayer);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(3, 3, 3, 3);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, 0, -1, -1);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, -1, -1, -1);
        upBtn = new QPushButton(musicPlayer);
        upBtn->setObjectName(QStringLiteral("upBtn"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/images/up-icon"), QSize(), QIcon::Normal, QIcon::Off);
        upBtn->setIcon(icon);
        upBtn->setIconSize(QSize(20, 20));

        verticalLayout_2->addWidget(upBtn);

        downBtn = new QPushButton(musicPlayer);
        downBtn->setObjectName(QStringLiteral("downBtn"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/images/down-icon"), QSize(), QIcon::Normal, QIcon::Off);
        downBtn->setIcon(icon1);
        downBtn->setIconSize(QSize(20, 20));

        verticalLayout_2->addWidget(downBtn);

        verticalSpacer_2 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_2);

        addBtn = new QPushButton(musicPlayer);
        addBtn->setObjectName(QStringLiteral("addBtn"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/images/add-icon"), QSize(), QIcon::Normal, QIcon::Off);
        addBtn->setIcon(icon2);
        addBtn->setIconSize(QSize(20, 20));

        verticalLayout_2->addWidget(addBtn);

        removeBtn = new QPushButton(musicPlayer);
        removeBtn->setObjectName(QStringLiteral("removeBtn"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/images/remove-icon"), QSize(), QIcon::Normal, QIcon::Off);
        removeBtn->setIcon(icon3);
        removeBtn->setIconSize(QSize(20, 20));

        verticalLayout_2->addWidget(removeBtn);

        saveBtn = new QPushButton(musicPlayer);
        saveBtn->setObjectName(QStringLiteral("saveBtn"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/images/save-icon"), QSize(), QIcon::Normal, QIcon::Off);
        saveBtn->setIcon(icon4);
        saveBtn->setIconSize(QSize(20, 20));

        verticalLayout_2->addWidget(saveBtn);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout_2->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 1, -1, -1);
        trackList = new QTableWidget(musicPlayer);
        if (trackList->columnCount() < 2)
            trackList->setColumnCount(2);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setTextAlignment(Qt::AlignLeft|Qt::AlignVCenter);
        trackList->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        __qtablewidgetitem1->setTextAlignment(Qt::AlignLeft|Qt::AlignVCenter);
        trackList->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        trackList->setObjectName(QStringLiteral("trackList"));
        trackList->setColumnCount(2);

        verticalLayout_3->addWidget(trackList);

        playerControls = new QHBoxLayout();
        playerControls->setSpacing(6);
        playerControls->setObjectName(QStringLiteral("playerControls"));
        playBtn = new QPushButton(musicPlayer);
        playBtn->setObjectName(QStringLiteral("playBtn"));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/images/play-icon"), QSize(), QIcon::Normal, QIcon::Off);
        playBtn->setIcon(icon5);
        playBtn->setIconSize(QSize(24, 24));

        playerControls->addWidget(playBtn);

        horizontalSpacerL = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        playerControls->addItem(horizontalSpacerL);

        prevBtn = new QPushButton(musicPlayer);
        prevBtn->setObjectName(QStringLiteral("prevBtn"));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/images/prev-icon"), QSize(), QIcon::Normal, QIcon::Off);
        prevBtn->setIcon(icon6);

        playerControls->addWidget(prevBtn);

        nextBtn = new QPushButton(musicPlayer);
        nextBtn->setObjectName(QStringLiteral("nextBtn"));
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/images/next-icon"), QSize(), QIcon::Normal, QIcon::Off);
        nextBtn->setIcon(icon7);

        playerControls->addWidget(nextBtn);

        horizontalSpacerR = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        playerControls->addItem(horizontalSpacerR);

        muteBtn = new QPushButton(musicPlayer);
        muteBtn->setObjectName(QStringLiteral("muteBtn"));
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/images/volume-icon"), QSize(), QIcon::Normal, QIcon::Off);
        muteBtn->setIcon(icon8);

        playerControls->addWidget(muteBtn);

        volumeSlider = new QSlider(musicPlayer);
        volumeSlider->setObjectName(QStringLiteral("volumeSlider"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(volumeSlider->sizePolicy().hasHeightForWidth());
        volumeSlider->setSizePolicy(sizePolicy);
        volumeSlider->setOrientation(Qt::Horizontal);

        playerControls->addWidget(volumeSlider);

        volumeLbl = new QLabel(musicPlayer);
        volumeLbl->setObjectName(QStringLiteral("volumeLbl"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(volumeLbl->sizePolicy().hasHeightForWidth());
        volumeLbl->setSizePolicy(sizePolicy1);
        volumeLbl->setMinimumSize(QSize(30, 0));
        volumeLbl->setAlignment(Qt::AlignCenter);

        playerControls->addWidget(volumeLbl);


        verticalLayout_3->addLayout(playerControls);

        durationBox = new QHBoxLayout();
        durationBox->setSpacing(6);
        durationBox->setObjectName(QStringLiteral("durationBox"));
        durationBox->setContentsMargins(-1, 0, -1, -1);
        progressLbl = new QLabel(musicPlayer);
        progressLbl->setObjectName(QStringLiteral("progressLbl"));

        durationBox->addWidget(progressLbl);

        progressSlider = new QSlider(musicPlayer);
        progressSlider->setObjectName(QStringLiteral("progressSlider"));
        progressSlider->setOrientation(Qt::Horizontal);

        durationBox->addWidget(progressSlider);

        durationLbl = new QLabel(musicPlayer);
        durationLbl->setObjectName(QStringLiteral("durationLbl"));

        durationBox->addWidget(durationLbl);


        verticalLayout_3->addLayout(durationBox);


        horizontalLayout_2->addLayout(verticalLayout_3);


        verticalLayout->addLayout(horizontalLayout_2);

        MainWindow->setCentralWidget(musicPlayer);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QString());
        upBtn->setText(QString());
        downBtn->setText(QString());
        addBtn->setText(QString());
        removeBtn->setText(QString());
        saveBtn->setText(QString());
        QTableWidgetItem *___qtablewidgetitem = trackList->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "Title Name", 0));
        QTableWidgetItem *___qtablewidgetitem1 = trackList->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "Duration", 0));
        playBtn->setText(QString());
        prevBtn->setText(QString());
        nextBtn->setText(QString());
        muteBtn->setText(QString());
        volumeLbl->setText(QApplication::translate("MainWindow", "0", 0));
        progressLbl->setText(QApplication::translate("MainWindow", "- : --", 0));
        durationLbl->setText(QApplication::translate("MainWindow", "- : --", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

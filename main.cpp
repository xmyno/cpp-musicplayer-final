#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setApplicationName("cppMusicPlayer");
    a.setOrganizationName("cpp14Beleg");
    a.setOrganizationDomain("hs-mittweida.de");
    a.setApplicationDisplayName("cppMusicPlayer");
    //a.setWindowIcon(QIcon(":/logo.png"));


    MainWindow w;
    w.show();

    return a.exec();
}

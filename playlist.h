#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QMap>
#include <QQueue>
#include <QMediaPlayer>
#include <QFile>

#include "track.h"


class Playlist : public QObject
{
    Q_OBJECT

signals:
    void mediaAdded(int index);
    void mediaRemoved(int index);
    void mediaMoved(int index, int offset);
    void trackDurationChanged(int index);
    void currentTrackChanged(int curIndex, int oldIndex);

public:
    Playlist();
    ~Playlist();

    int mediaAmount() { return _mediaAmount; }
    QMap<int, Track*> *getPlaylist() { return ptracks; }


    void addMedia(const QString &filePath);
    void removeMedia(int index);
    void moveMedia(int index, int offset);

    // Gibt den Track an gewünschtem Index zurück
    Track *atIndex(int index) { return ptracks->value(index); }


    void previous();
    void next();

    // Gibt den aktuellen Index zurück
    int currentIndex() { return _currentIndex; }
    // Setzt den aktuellen Index
    void setCurrentIndex(int index) { _currentIndex = index; }

    void tidyPlaylist();
    void checkDuration();

private:

    int _currentIndex;
    int _mediaAmount;
    QMap<int, Track*> *ptracks;

    QMediaPlayer durationChecker;
    QQueue<Track*> *trackQueue;

private slots:
    void updateTrackDuration(qint64 duration);

};

#endif // PLAYLIST_H

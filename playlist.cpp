#include "playlist.h"

#include <QFile>
#include <QFileDialog>
#include <QMessageBox>

/* -------------------------------------------
 * Playlist für die Verwendung mit QMediaPlayer
 *
 * ----------------------------------------- */
Playlist::Playlist() :
    _currentIndex(0),
    _mediaAmount(0),
    ptracks(new QMap<int, Track*>),
    durationChecker(0),
    trackQueue(new QQueue<Track*>)
{

    // durationChanged-Event des QMediaPlayer's, der für die Track-Initialisierung verwendet wird
    connect(&durationChecker, SIGNAL(durationChanged(qint64)), this, SLOT(updateTrackDuration(qint64)));

}

// Deconstructor
Playlist::~Playlist() {
    delete ptracks;
    delete trackQueue;
}


/* -------------------------------------------
 * Track zur Playlist hinzufügen
 *
 * ----------------------------------------- */
void Playlist::addMedia(const QString &filePath) {

    // Dateipfad des Tracks setzen
    QString *pfilePath = new QString(filePath);

    // den Name der Datei aus dem Pfad heraustrennen (Pfad + Dateiendung entfernen)
    QString name = QDir::toNativeSeparators(filePath); // Slashes vereinheitlichen
    name.remove(0, name.lastIndexOf(QDir::separator()) + 1); // Pfad
    name.chop(name.length() - name.lastIndexOf(".")); // Endung

    // Name des Tracks setzen
    QString *pfilename = new QString(name);

    // Neuen Track anlegen
    Track *pTrack = new Track(pfilePath, pfilename, _mediaAmount);

    // Track in die Playlist einfügen
    ptracks->insert(_mediaAmount, pTrack);
    // Track der Queue hinzufügen, die neue Tracks initialisiert
    trackQueue->append(pTrack);

    // Anzahl Tracks hochzählen
    _mediaAmount++;

    // Signal senden, dass ein neuer Track hinzufügt wurde (mit Index des Tracks)
    emit mediaAdded(_mediaAmount - 1);

}

/* -------------------------------------------
 * Track aus Playlist entfernen
 *
 * ----------------------------------------- */
void Playlist::removeMedia(int index) {

    // Existiert der Index nicht, dann tue nichts
    if (!ptracks->contains(index)) return;

    // Hole den Track aus der Playlist
    Track *track = ptracks->value(index);
    // Entferne des Track aus der Playlist
    ptracks->remove(index);
    // Lösche das Track-Objekt
    delete track;

    // Wenn es nicht der letzte Track in der Playlist war, dann räume die Playlist auf (Lücken schließen)
    if (index != _mediaAmount - 1)
        tidyPlaylist();

    // Anzahl Tracks runterzählen
    _mediaAmount--;

    // Signal senden, dass ein Track aus der Playlist entfernt wurde (mit Index des Tracks)
    emit mediaRemoved(index);

}

/* -------------------------------------------
 * Position von Tracks in der Playlist verschieben
 *
 * ----------------------------------------- */
void Playlist::moveMedia(int index, int offset) {

    // Tracks vertauschen
    Track *mvtrack = ptracks->value(index + offset);
    // Track an der zu vertauschenden Position (darüber oder darunter) auf aktuelle Position schreiben
    ptracks->insert(index + offset, ptracks->value(index));
    ptracks->insert(index, mvtrack);

    // Track an neue Position schreiben, entweder darüber oder darunter
    if (offset >= 0) {
        ptracks->value(index)->changeIndex(-offset);
        ptracks->value(index + offset)->changeIndex(offset);
    }
    else {
        ptracks->value(index)->changeIndex(offset);
        ptracks->value(index + offset)->changeIndex(-offset);
    }

    // Signal senden, dass ein Track bewegt wurde (mit Index des Tracks und der Richtung der Verschiebung)
    emit mediaMoved(index, offset);

}

/* -------------------------------------------
 * Playlist nach Löschungen aufräumen
 *
 * ----------------------------------------- */
void Playlist::tidyPlaylist() {

    // beim 2. Element starten, da immer nach einer Lücke "über" dem aktuellen Element in der Map geschaut wird
    int cur = 1;
    while (cur < _mediaAmount) {

        // leere "Reihen" in der Map füllen
        while (cur > 0 && !ptracks->contains(cur - 1)) {

            // das aktuelle Element in der Map eine "Reihe" nach oben schieben
            ptracks->insert(cur - 1, ptracks->value(cur));

            // den Index des Tracks synchron mit der Playlist halten
            ptracks->value(cur - 1)->changeIndex(-1);

            // das verschobene Element löschen
            ptracks->remove(cur);

            // wieder auf vorheriges Element setzen, falls die Lücke mehr als eine Reihe groß ist
            cur--;

        }

        cur++;
    }

}

/* -------------------------------------------
 * Startet
 *
 * ----------------------------------------- */
void Playlist::checkDuration() {

    // Tue nichts, wenn die Track-Queue leer ist
    if (trackQueue->isEmpty())
        return;

    // Hole den ersten Track in der Queue, aber lasse ihn noch in der Queue
    Track *track = trackQueue->first();

    // Lade den Track in den QMediaPlayer
    durationChecker.setMedia(QUrl::fromLocalFile( *(track->filePath()) ));

    // Wenn der QMediaPlayer den Track fertig geladen hat, wird die updateTrackDuration Methode
    // über das durationChanged-Event aufgerufen

}

/* -------------------------------------------
 * Slot für das durationChanged-Event des QMediaPlayer's für Track-Initialisierung
 *
 * Schreibt die Dauer des Tracks in das Track-Objekt
 * ----------------------------------------- */
void Playlist::updateTrackDuration(qint64 duration) {

    // Track wurde nicht richtig initialisiert, springe zum nächsten in der Queue
    if (!duration > 0) {
        trackQueue->removeFirst();
        checkDuration();
    }

    // Hole den ersten Track aus der Queue, diesmal aber aus der Queue löschen
    Track *track = trackQueue->takeFirst();
    // Die Dauer des Tracks im Track-Objekt setzen
    track->setDuration((int) duration);

    // Signal senden, dass die Dauer eines Tracks eingefügt bzw. geändert wurde
    emit trackDurationChanged(track->index());

    // "rekursiv" den nächsten Track zur Überprüfung aufrufen, damit der nächste Track erst
    // überprüft wird, wenn der vorherige fertig ist
    checkDuration();

}

/* -------------------------------------------
 * Nächsten Track der Playlist abspielen
 *
 * ----------------------------------------- */
void Playlist::next() {

    // Vorgänger-Index speichern, für Aktualisierung der Playlist-Tabelle
    int oldIndex = _currentIndex;

    // Ist aktuell der letzte Track, dann springe wieder an den Anfang der Playlist
    if (_currentIndex == _mediaAmount - 1)
        _currentIndex = 0;
    // sonst ganz normal den nächsten Track abspielen
    else
        _currentIndex++;

    // Signal senden, dass sich der aktuelle Track geändert hat
    emit currentTrackChanged(_currentIndex, oldIndex);

}

/* -------------------------------------------
 * Vorherigen Track der Playlist spielen
 *
 * ----------------------------------------- */
void Playlist::previous() {

    // Vorgänger-Index speichern, für Aktualisierung der Playlist-Tabelle
    int oldIndex = _currentIndex;

    // Ist aktuell der erste Track, dann springe ans den Ende der Playlist
    if (_currentIndex == 0)
        _currentIndex = _mediaAmount - 1;
    // ansonsten einfach den vorherigen Track abspielen
    else
        _currentIndex--;

    // Signal senden, dass sich der aktuelle Track geändert hat
    emit currentTrackChanged(_currentIndex, oldIndex);

}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QString>
#include <QScopedPointer>

#include "playlist.h"
#include "track.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void loadMediaAtCurrentPlaylistIndex();
    QString buildDurationString(int duration);

private slots:
    // Button slots
    void on_upBtn_clicked();
    void on_downBtn_clicked();

    void on_addBtn_clicked();
    void on_removeBtn_clicked();
    void on_saveBtn_clicked();

    void on_prevBtn_clicked();
    void on_playBtn_clicked();
    void on_nextBtn_clicked();

    void on_muteBtn_clicked();

    // MediaPlayer Event Slots
    void updateTrackDuration(qint64 duration);
    void updateTrackProgress(qint64);
    void updatePlayButtonState(QMediaPlayer::State state);
    void errorOccured(QMediaPlayer::Error);

    void trackProgressChanged(int value);

    void playTrack();
    void playNext(QMediaPlayer::MediaStatus status);
    void volumeChanged(int value);

    // Playlist Event Slots
    void tracklistAppendRow(int index);
    void tracklistDeleteRowAtIndex(int index);
    void moveRows(int offset);
    void tracklistSwapRows(int index, int offset);
    void updateDurationColumn(int index);
    void updateCurrentTrackRow(int curIndex, int oldIndex);
    void handleRowDoubleClick(int index);

private:
    Ui::MainWindow *ui;

    QMediaPlayer player;
    Playlist playlist;

    bool playerMuted;

};




#endif // MAINWINDOW_H

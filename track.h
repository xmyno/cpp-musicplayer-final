#ifndef TRACK_H
#define TRACK_H

#include <QString>

class Track
{

public:
    Track(QString *filePath, QString *fileName, int index);
    ~Track();

    int index() { return _index; }
    QString *filePath() { return pfilePath; }
    QString *name() { return pName; }
    int duration() { return _duration; }

    // Anpassung des Index bei Verschiebung des Tracks
    void changeIndex(int offset) { _index += offset; }
    // Setzen der Dauer eines Tracks
    void setDuration(int duration) { _duration = duration; }

private:

    int _index;
    QString *pfilePath;
    QString *pName;
    int _duration;

};

#endif // TRACK_H

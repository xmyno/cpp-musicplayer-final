#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTime>

/* -------------------------------------------
 * GUI Hauptfenster
 *
 * Erstellung und Initialisierung des Hauptfensters.
 * ----------------------------------------- */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    player(0),
    playlist(),
    playerMuted(false)
{
    ui->setupUi(this);

    // Einstellungen für das QTableWidget, welches die Playlist anzeigt
    ui->trackList->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->trackList->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Interactive);

    ui->trackList->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->trackList->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->trackList->setStyleSheet("selection-background-color: rgba(128,128,128,50);");

    // Doppelklick auf einen Tabelleneintrag
    connect(ui->trackList, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(handleRowDoubleClick(int)));

    // Signale des QMediaPlayer's an Slots binden
    connect(&player, SIGNAL(durationChanged(qint64)), this, SLOT(updateTrackDuration(qint64)));
    connect(&player, SIGNAL(positionChanged(qint64)), this, SLOT(updateTrackProgress(qint64)));
    connect(&player, SIGNAL(stateChanged(QMediaPlayer::State)), this, SLOT(updatePlayButtonState(QMediaPlayer::State)));
    connect(&player, SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)), this, SLOT(playNext(QMediaPlayer::MediaStatus)));
    connect(&player, SIGNAL(error(QMediaPlayer::Error)), this, SLOT(errorOccured(QMediaPlayer::Error)));

    // Änderung des Track-Progress Sliders
    connect(ui->progressSlider, SIGNAL(valueChanged(int)), this, SLOT(trackProgressChanged(int)));

    // Signale der Playlist-Klasse
    connect(&playlist, SIGNAL(mediaAdded(int)), this, SLOT(tracklistAppendRow(int)));
    connect(&playlist, SIGNAL(mediaRemoved(int)), this, SLOT(tracklistDeleteRowAtIndex(int)));
    connect(&playlist, SIGNAL(mediaMoved(int, int)), this, SLOT(tracklistSwapRows(int, int)));
    connect(&playlist, SIGNAL(trackDurationChanged(int)), this, SLOT(updateDurationColumn(int)));

    // Aktiven Track in der Tabelle hervorheben
    connect(&playlist, SIGNAL(currentTrackChanged(int, int)), this, SLOT(updateCurrentTrackRow(int, int)));

    // Volume Slider Signal und Einstellungen
    connect(ui->volumeSlider, SIGNAL(valueChanged(int)), this, SLOT(volumeChanged(int)));
    ui->volumeSlider->setRange(0, 100);
    ui->volumeSlider->setValue(50);


}

// Destructor des GUI Hauptfensters
MainWindow::~MainWindow()
{
    delete ui;
}


/* -------------------------------------------
 * PLAYLIST GUI CONTROLS
 *
 * beinhaltet:
 *  - 'Add' Button (eine .mp3 oder .pls Datei hinzufügen)
 *  - 'Remove' Button (selektierte Tracks aus der Playlist entfernen)
 *  - 'Up' Button (selektierte Tracks eine Position nach oben verschieben)
 *  - 'Down' Button (selektierte Tracks eine Position nach unten verschieben)
 *  - 'Save' Button (aktuelle Playlist speichern)
 *
 * ----------------------------------------- */


/* -------------------------------------------
 * Add Button
 *
 * Öffnet einen File Dialog und lässt den Benutzer eine .mp3 oder .pls Datei auswählen.
 * Ist es eine .mp3-Datei wird sie als Track der Playlist hinzugefügt.
 * Ist es eine .pls-Datei wird geprüft, ob sie einen korrekten Syntax hat
 * und danach werden die beinhalteten Tracks der Playlist hinzufügt.
 *
 * ----------------------------------------- */
void MainWindow::on_addBtn_clicked()
{

    // File Dialog öffnen und den Benutzer .mp3- oder .pls-Dateien auswählen lassen
    QStringList filePaths = QFileDialog::getOpenFileNames(this, tr("Open text document"), QDir::homePath(), tr("MP3 or Playlist file (*.mp3 *.pls)"));

    int mediaAmount = playlist.mediaAmount();
    // Alle ausgewählten Dateipfade durchlaufen
    foreach (QString filePath, filePaths) {

        if (!filePath.isEmpty()) {

            // Dateiendung von Dateipfad abtrennen, um zwischen Lied und Playlist unterscheiden zu können
            QString ending = filePath;
            ending.remove(0, ending.lastIndexOf(".") + 1); // Pfad

            // Bei einer .mp3-Datei einfach der Playlist hinzufügen
            if (ending == "mp3") {
                playlist.addMedia(filePath);
            }
            // Bei einer .pls-Datei den Inhalt der Datei parsen
            else if (ending == "pls") {

                // Playlist-Datei öffnen
                QFile file(filePath);
                if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
                    continue;

                QTextStream ts(&file);

                // Erste Zeile auf den String '[playlist]' überprüfen
                QString line = ts.readLine();
                if (line != "[playlist]")
                    continue;

                // Nach Tracks in der Playlist suchen und der Playlist hinzufügen.
                while (!ts.atEnd()) {

                    line = ts.readLine();

                    // Fängt die Zeile mit 'File' an?
                    if (line.indexOf("File", 0, Qt::CaseSensitive) == 0) {
                        line.remove(0, line.indexOf("=", 0) + 1);
                        playlist.addMedia(line);
                    }
                }
            }

        }
    }

    // Beim ersten mal Hinzufügen den momentan aktiven Track markieren
    if (mediaAmount == 0)
        updateCurrentTrackRow(0, -1);
    // Nach dem Hinzufügen von Dateien die Dauer der Tracks für die Anzeige überprüfen
    playlist.checkDuration();

}

/* -------------------------------------------
 * Remove Button
 *
 * Löscht selektierte Tracks aus der Playlist.
 *
 * ----------------------------------------- */
void MainWindow::on_removeBtn_clicked()
{

    // In einer Schleife alle selektierten Reihen des QTableWidgets durchlaufen
    foreach (QTableWidgetItem *item, ui->trackList->selectedItems()) {
        playlist.removeMedia(item->row());
    }

}

/* -------------------------------------------
 * Up Button
 *
 * Verschiebt selektierte Tracks der Playlist um eine Stelle nach oben.
 * ----------------------------------------- */
void MainWindow::on_upBtn_clicked()
{
    // Reihen nach oben schieben (offset -1 = nach oben)
    moveRows(-1);
}

/* -------------------------------------------
 * Down Button
 *
 * Verschiebt selektierte Tracks der Playlist um eine Stelle nach unten.
 * ----------------------------------------- */
void MainWindow::on_downBtn_clicked()
{
    // Reihen nach unten schieben (offset 1 = nach unten)
    moveRows(1);
}

/* -------------------------------------------
 * Save Button
 *
 * Speichert die aktuelle Playlist in eine Datei
 * ----------------------------------------- */
void MainWindow::on_saveBtn_clicked()
{

    // File Dialog öffnen in dem der Benutzer einen Dateinamen festlegen kann
    QString fileName = QFileDialog::getSaveFileName(this, "Save playlist", QDir::homePath(), "Playlist (*.pls)");

    // Kein Dateiname eingegeben
    if (fileName.isEmpty()) {
        QMessageBox::warning(this, "Invalid File Name Error", "No valid file name was entered.", QMessageBox::Ok, QMessageBox::Ok);
        return;
    }

    // Datei zum Schreiben öffnen
    QFile file (fileName);
    file.open(QIODevice::WriteOnly | QIODevice::Text);

    // Datei konnte nich zum Schreiben geöffnet werden
    if (!file.isOpen()) {
        QMessageBox::warning(this, "File Save Error", "The playlist could not be saved.", QMessageBox::Ok, QMessageBox::Ok);
        return;
    }

    QTextStream ts(&file);

    ts << "[playlist]" << endl;

    // Alle Tracks in die Datei schreiben
    int index = 1;
    foreach (Track *track, *(playlist.getPlaylist())) {

        QString path = *(track->filePath());
        ts << "File" << index << "=" << path << endl;

        QString name = *(track->name());
        ts << "Name" << index << "=" << name << endl;

        int duration = ( track->duration() ) / 1000;
        ts << "Length" << index << "=" << duration << endl;

        index++;

    }

    ts << "NumberOfEntries=" << playlist.mediaAmount() << endl;
    ts << "Version=2" << endl;

    // Datei schließen
    file.close();

}


/* -------------------------------------------
 * MEDIA PLAYER GUI CONTROLS
 *
 * beinhaltet:
 *  - 'Play' Button (momentan aktiven Track spielen oder pausieren)
 *  - 'Next' Button (nächsten Track als aktiven Track setzen)
 *  - 'Previous' Button (vorherigen Track als aktiven Track setzen)
 *  - Doppelklick-Slot der Playlist-Tabelle (angeklickten Track abspielen)
 *
 * ----------------------------------------- */


/* -------------------------------------------
 * Play Button
 *
 * Aktiven Track abspielen
 * ----------------------------------------- */
void MainWindow::on_playBtn_clicked()
{

    // Sind Lieder in der Playlist vorhanden?
    if (playlist.mediaAmount() == 0)
        return;

    // In welchem Zustand befindet sich der QMediaPlayer?
    QMediaPlayer::State state = player.state();

    // Pause: die Wiedergabe fortsetzen
    if (state == QMediaPlayer::PausedState) {
        player.play();
    }
    // Stop: aktiven Track laden und abspielen
    else if (state == QMediaPlayer::StoppedState) {
        playTrack();
    }
    // Play: die Wiedergabe pausieren
    else if (state == QMediaPlayer::PlayingState) {
        player.pause();
    }

}

/* -------------------------------------------
 * Previous Button
 *
 * Vorherigen Track in der Playlist als aktiven Track setzen.
 * ----------------------------------------- */
void MainWindow::on_prevBtn_clicked()
{

    // Sind Lieder in der Playlist vorhanden?
    if (playlist.mediaAmount() == 0)
        return;

    // Den vorheringen Track in der Playlist als aktiven Track setzen
    playlist.previous();

    // Nur wenn momentan bereits ein Track abgespielt wird sofort den neuen Track abspielen
    if (player.state() == QMediaPlayer::PlayingState)
        playTrack();

}

/* -------------------------------------------
 * Next Button
 *
 * Nächsten Track in der Playlist als aktiven Track setzen.
 * ----------------------------------------- */
void MainWindow::on_nextBtn_clicked()
{

    // Sind Lieder in der Playlist vorhanden?
    if (playlist.mediaAmount() == 0)
        return;

    // Den nächsten Track in der Playlist als aktiven Track setzen
    playlist.next();

    // Nur wenn momentan bereits ein Track abgespielt wird sofort den neuen Track abspielen
    if (player.state() == QMediaPlayer::PlayingState)
        playTrack();

}

/* -------------------------------------------
 * DoubleClick Slot der Playlist Tabelle
 *
 * Angeklickten Track abspielen
 * ----------------------------------------- */
void MainWindow::handleRowDoubleClick(int index) {

    // Aktiven Track in der Tabelle markieren
    updateCurrentTrackRow(-1, playlist.currentIndex());
    // Playlist-Index auf die gewählte Stelle setzen
    playlist.setCurrentIndex(index);
    // Track laden und abspielen
    playTrack();

}


/* -------------------------------------------
 * WIEDERGABE VON TRACKS
 *
 * Alles was mit der Wiedergabe von Tracks und deren
 * Aktualisierung in der GUI zu tun hat.
 *
 * ----------------------------------------- */


/* -------------------------------------------
 * Track Wiedergabe
 *
 * Lädt den momentan aktiven Track der Playlist und spielt ihn ab.
 * ----------------------------------------- */
void MainWindow::playTrack() {

    // aktiven Track laden
    loadMediaAtCurrentPlaylistIndex();

    // aktive Zeile in der Playlist-Tabelle markieren
    int index = playlist.currentIndex();
    updateCurrentTrackRow(index, -1);
    // Fenstertitel anpassen
    setWindowTitle(QString( *(playlist.atIndex(index)->name()) ));

    // Wiedergabe starten
    player.play();

}

/* -------------------------------------------
 * Slot für das mediaStatusChanged-Event des QMediaPlayer's
 *
 * Bei Ende des laufenden Tracks wird der nächste Track abgespielt werden.
 * ----------------------------------------- */
void MainWindow::playNext(QMediaPlayer::MediaStatus status) {

    if (status == QMediaPlayer::EndOfMedia) {
        playlist.next();
        playTrack();
    }

}

/* -------------------------------------------
 * Slot für das error-Event des QMediaPlayer's
 *
 * Bei einem Resource-Error wird in der Playlist-Tabelle kenntlich gemacht,
 * welcher Track nicht geladen werden konnte.
 * ----------------------------------------- */
void MainWindow::errorOccured(QMediaPlayer::Error error) {

    if (error == player.ResourceError) {

        // Text der Dauer-Spalte ändern und Farbe auf rot setzen
        ui->trackList->item(playlist.currentIndex(), 1)->setText("File missing");
        ui->trackList->item(playlist.currentIndex(), 1)->setForeground(QColor::fromRgb(230,10,10));

        // Gibt es mehr als einen Track, dann spiele den nächsten ab.
        if (playlist.mediaAmount() > 1)
            playlist.next();
    }

}

/* -------------------------------------------
 * Slot für das stateChanged-Event des QMediaPlayer's
 *
 * Entsprechend des aktuellen Zustands des Players wird das Icon
 * des Play-Buttons entweder auf Play oder Pause gesetzt.
 * ----------------------------------------- */
void MainWindow::updatePlayButtonState(QMediaPlayer::State state) {

    // Stop und Pause: Play Icon anzeigen
    if (state == QMediaPlayer::PausedState || state == QMediaPlayer::StoppedState) {
        ui->playBtn->setIcon(QIcon(tr(":/images/play-icon")));
    }
    // Play: Pause Icon anzeigen
    else if (state == QMediaPlayer::PlayingState) {
        ui->playBtn->setIcon(QIcon(tr(":/images/pause-icon")));
    }

}

/* -------------------------------------------
 * Slot für das durationChanged-Event des QMediaPlayer's
 *
 * Bei Änderung der Dauer des aktiven Tracks das Dauer-Label und den
 * Track-Progress Slider auf aktuelle Werte anpassen.
 * ----------------------------------------- */
void MainWindow::updateTrackDuration(qint64 duration) {

    // Duration Label auf Dauer des aktuellen Tracks setzen
    ui->durationLbl->setText(buildDurationString(duration));

    // Track-Progress Slider Werte auf Dauer des aktuellen Tracks anpassen
    ui->progressSlider->setRange(0, duration);
    ui->progressSlider->setEnabled(duration > 0);
    ui->progressSlider->setPageStep(duration / 20);

}

/* -------------------------------------------
 * Slot für das positionChanged-Event des QMediaPlayer's
 *
 * Aktualisiert den Fortschritt der Wiedergabe im Fortschritt-Label
 * und dem Track-Progress Slider.
 * ----------------------------------------- */
void MainWindow::updateTrackProgress(qint64 progress){

    // Label und Slider des aktuellen Wiedergabefortschritts aktualisieren
    ui->progressLbl->setText(buildDurationString(progress));

    ui->progressSlider->setValue(progress);

}

/* -------------------------------------------
 * Slot für das valueChanged-Event des Track-Progress Sliders
 *
 * Nur bei einer größeren Änderung des Fortschritt-Wertes die Abspielposition
 * des Tracks verändern, da es sonst zu unerwünschten Effekten bei der normalen
 * Wiedergabe kommt (kleine Schritte durch das Update in updateTrackProgress() ).
 * ----------------------------------------- */
void MainWindow::trackProgressChanged(int value) {

    if ( qAbs(player.position() - value) > 99 )
        player.setPosition(value);

}


/* -------------------------------------------
 * TRACKS HINZUFÜGEN
 *
 * ----------------------------------------- */


/* -------------------------------------------
 * Slot für das mediaAdded-Event der Playlist Klasse
 *
 * Beim Hinzufügen eines neuen Tracks einen Eintrag ans Ende der Playlist-Tabelle machen.
 * ----------------------------------------- */
void MainWindow::tracklistAppendRow(int index) {

    // Neue Zeile erstellen
    ui->trackList->insertRow(index);

    // Tabellen-Zelle für den Track Titel anlegen und anzeigen
    QTableWidgetItem *title = new QTableWidgetItem( *(playlist.atIndex(index)->name()) , 0);
    title->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    ui->trackList->setItem(index, 0, title);

    // Tabellen-Zelle für die Track Dauer anlegen und anzeigen (richtige Dauer wird nach erfolgreichem Laden dargestellt)
    QTableWidgetItem *duration = new QTableWidgetItem(QString("-:--"), 0);
    duration->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    ui->trackList->setItem(index, 1, duration);

}

/* -------------------------------------------
 * Slot für das trackDurationChanged-Event der Playlist Klasse
 *
 * Playlist-Tabelleneintrag aktualisieren, wenn ein Track fertig initialisiert wurde
 * und die Dauer bekannt ist.
 * ----------------------------------------- */
void MainWindow::updateDurationColumn(int index) {

    // Dauer-String erstellen
    QString durationString = buildDurationString(playlist.atIndex(index)->duration());

    // Tabellen-Zelle des Tracks aktualisieren
    ui->trackList->item(index, 1)->setText(durationString);

}


/* -------------------------------------------
 * TRACKS LÖSCHEN
 *
 * ----------------------------------------- */


/* -------------------------------------------
 * Slot für das mediaRemoved-Event der Playlist Klasse
 *
 * Beim Löschen eines neuen Tracks den Eintrag aus der Playlist-Tabelle entfernen.
 * ----------------------------------------- */
void MainWindow::tracklistDeleteRowAtIndex(int index) {

    // Zeile des gelöschten Tracks entfernen
    ui->trackList->removeRow(index);

}


/* -------------------------------------------
 * TRACKS VERSCHIEBEN
 *
 * ----------------------------------------- */


/* -------------------------------------------
 * Slot für das mediaAdded-Event der Playlist Klasse
 *
 * Beim Hinzufügen eines neuen Tracks einen Eintrag ans Ende der Playlist-Tabelle machen.
 * ----------------------------------------- */
void MainWindow::moveRows(int offset) {

    QMap<int, int> indices;

    // Items werden in der Reihenfolge zurückgegeben, in der sie selektiert worden
    // Für korrektes Verschieben müssen die Reihen geordnet sein
    QList<QTableWidgetItem*> items = ui->trackList->selectedItems();

    foreach (QTableWidgetItem *item, items) {
        // Reihen in eine Map schreiben, damit die Indizes geordnet sind
        if (!indices.contains(item->row()))
            indices.insert(item->row(), item->row());
    }

    // Selektierung aufheben und Mode auf MultiSelection setzen, damit vom Code aus
    // die verschobenen Reihen wieder richtig markiert werden können
    ui->trackList->clearSelection();
    ui->trackList->setSelectionMode(QAbstractItemView::MultiSelection);

    QMapIterator<int, int> iter (indices);

    int prevIndex = -1;
    bool rowIsAtEdge = false;

    // Es wird nach oben verschoben
    if (offset == -1) {
        // Gibt es weitere Elemente in der Map?
        while (iter.hasNext()) {
            iter.next();

            // Bei nach oben verschieben muss geschaut werden, ob die erste Tabellenreihe selektiert ist
            if (iter.value() == 0)
                rowIsAtEdge = true;

            // oder es gibt keine Lücke zwischen 1. Reihe und der derzeitigen Reihe
            else if (iter.value() != prevIndex + 1)
                rowIsAtEdge = false;

            // Nur wenn die Reihe nicht an der oberen "Kante" ist um eine Stelle nach oben verschieben
            if (!rowIsAtEdge) {
                playlist.moveMedia(iter.value(), offset);

                // Aktuellen Index der Playlist anpassen, wenn die aktuelle Reihe verschoben wurde
                if (playlist.currentIndex() == iter.value())
                    playlist.setCurrentIndex(playlist.currentIndex() + offset);
            }
            else
                // Reihe wieder markieren, auch wenn sie nicht verschoben wurde
                ui->trackList->selectRow(iter.value());
            // Index speichern, für Überprüfung ob Reihe an der Kante liegt
            prevIndex = iter.value();
        }
    }
    // Das Gleiche für nach unten verschieben
    else if (offset == 1) {
        // Map von hinten durchlaufen
        iter.toBack();
        while (iter.hasPrevious()) {
            iter.previous();

            // Ist die selektierte Reihe die letzte Reihe der Liste?
            if (iter.value() == playlist.mediaAmount() - 1)
                rowIsAtEdge = true;
            // Oder gibt es keine Lücke zwischen der aktuellen und der letzten Reihe?
            else if (iter.value() != prevIndex - 1)
                rowIsAtEdge = false;

            // Nur verschieben wenn Reihe(n) nicht am Ende der Liste sind
            if (!rowIsAtEdge) {
                playlist.moveMedia(iter.value(), offset);

                if (playlist.currentIndex() == iter.value())
                    playlist.setCurrentIndex(playlist.currentIndex() + offset);
            }
            else
                ui->trackList->selectRow(iter.value());
            prevIndex = iter.value();
        }
    }

    // Markierung wieder auf den "User"-Mode setzen
    ui->trackList->setSelectionMode(QAbstractItemView::ExtendedSelection);

}

/* -------------------------------------------
 * Reihentausch für Verschiebung von Reihen
 *
 * Reihen vertauschen und "verschobene" Reihe wieder selektieren
 * ----------------------------------------- */
void MainWindow::tracklistSwapRows(int index, int offset) {

    // Inhalte zwischenspeichern
    QTableWidgetItem *titleOrigin = ui->trackList->takeItem(index, 0);
    QTableWidgetItem *durOrigin = ui->trackList->takeItem(index, 1);
    QTableWidgetItem *titleTarget = ui->trackList->takeItem(index + offset, 0);
    QTableWidgetItem *durTarget = ui->trackList->takeItem(index + offset, 1);

    // Vertauschte Werte wieder in die Tabelle schreiben
    ui->trackList->setItem(index, 0, titleTarget);
    ui->trackList->setItem(index, 1, durTarget);
    ui->trackList->setItem(index + offset, 0, titleOrigin);
    ui->trackList->setItem(index + offset, 1, durOrigin);

    // Verschobene Reihe wieder selektieren
    ui->trackList->selectRow(index + offset);

}


/* -------------------------------------------
 * LAUTSTÄRKE
 *
 * ----------------------------------------- */


/* -------------------------------------------
 * Stummschalten Button
 *
 * Bei Klick wird der QMediaPlayer stumm geschalten
 * ----------------------------------------- */
void MainWindow::on_muteBtn_clicked()
{
    // Player ist nicht gemutet -> muten
    if (!playerMuted) {

        playerMuted = true;
        player.setVolume(0);
        ui->muteBtn->setIcon(QIcon(tr(":/images/mute-icon")));

    }
    else {

        playerMuted = false;
        player.setVolume(ui->volumeSlider->value());
        ui->muteBtn->setIcon(QIcon(tr(":/images/volume-icon")));

    }
}

/* -------------------------------------------
 * Slot für das valueChanged-Event des Lautstärke-Sliders
 *
 * Lautstärke des QMediaPlayer's auf den Wert des Sliders setzen (0-100).
 * ----------------------------------------- */
void MainWindow::volumeChanged(int value) {

    // Ist der Player bei Änderung des Sliders muted, dann entmuten
    if (playerMuted) {
        playerMuted = false;
        ui->muteBtn->setIcon(QIcon(tr(":/images/volume-icon")));
    }

    // Lautstärke-Label aktualisieren
    ui->volumeLbl->setText(QString("%1%").arg(value));
    // Lautstärke setzen
    player.setVolume(value);

}


/* -------------------------------------------
 * ALLGEMEINE FUNKTIONEN
 *
 * ----------------------------------------- */


/* -------------------------------------------
 * Dauer-String für Tracklänge
 *
 * Baut aus der Dauer der Tracks einen String im Format (H:mm:ss, oder m:ss bei < 1 Stunde Dauer)
 * ----------------------------------------- */
QString MainWindow::buildDurationString(int duration) {

    // Dauer liegt in Millisekunden vor
    int h = duration / 3600000;
    int m = ((duration / 1000) % 3600) / 60;
    int s = (duration / 1000) % 60;

    // QTime hat die Möglichkeit der Erstellung von Strings, Zeitformat wird der toString()-Methode übergeben
    QTime time (h, m, s);
    if (h > 0)
        return time.toString(tr("H:mm:ss"));
    else
        return time.toString(tr("m:ss"));

}

/* -------------------------------------------
 * Aktuellen Track der Playlist laden
 *
 * ----------------------------------------- */
void MainWindow::loadMediaAtCurrentPlaylistIndex() {

    // filePath des Tracks am aktuellen Index der Playlist holen
    // und dem QMediaPlayer zum Laden übergeben
    player.setMedia(QUrl::fromLocalFile(
                        *( playlist.atIndex(playlist.currentIndex())->filePath()) )
                    );

}

/* -------------------------------------------
 * Aktuell abgespielten Track in der Playlist-Tabelle fett markieren
 * Vorherige fett-Markierung entfernen
 * ----------------------------------------- */
void MainWindow::updateCurrentTrackRow(int curIndex, int oldIndex) {

    QFont bold;
    bold.setBold(true);

    QFont normal;
    normal.setBold(false);

    // fett-Markierung des Tracks am vorherigen Index wieder entfernen
    if (oldIndex != -1) {
        ui->trackList->item(oldIndex, 0)->setFont(normal);
        ui->trackList->item(oldIndex, 1)->setFont(normal);
    }
    // aktuellen Track fett markieren
    if (curIndex != -1) {
        ui->trackList->item(curIndex, 0)->setFont(bold);
        ui->trackList->item(curIndex, 1)->setFont(bold);
    }

}

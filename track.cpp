#include "track.h"

#include <QDir>

/* -------------------------------------------
 * Tracks die in einer Playlist enthalten sind
 *
 * _index - der Index des Tracks in der Playlist
 * pfilePath - der Pfad der Datei
 * pName - Name des Tracks, aus dem Dateipfad entnommen (Name der Datei)
 * _duration - Dauer des Tracks in Millisekunden
 *
 * ----------------------------------------- */
Track::Track(QString *filePath, QString *fileName, int index) :
    _index(index),
    pfilePath(filePath),
    pName(fileName),
    _duration(0)
{

}

// Deconstructor
Track::~Track() {
    delete pfilePath;
    delete pName;
}
